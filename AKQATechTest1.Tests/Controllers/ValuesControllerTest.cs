﻿using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AKQATechTest1.Controllers;
using AKQATechTest1.Models;
using AKQATechTest1.Services.Contracts;
using log4net;
using Moq;

namespace AKQATechTest1.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void GetReturnsWithEmptyValue()
        {
            // Arrange
            var logger = new Mock<ILog>();

            var entryService = new Mock<IConvertService>();
            entryService.Setup(x => x.GetEntryValue(It.IsAny<string>())).Returns("");

            // set controller object
            var controller = new ValuesController(entryService.Object, logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "1000uyuy"
            };

            #endregion

            // Act
            var actionResult = controller.Post(data);
            var contentResult = actionResult as OkNegotiatedContentResult<EntryResultViewModel>;

            // Assert
            Assert.IsNotNull(contentResult); // return data object should not be null
            Assert.IsNotNull(contentResult.Content); // return data content object should not be null
            Assert.AreEqual("", contentResult.Content.Number); // return number should be empty
        }


        [TestMethod]
        public void GetReturnsWithWords()
        {
            // Arrange
            var logger = new Mock<ILog>();

            var entryService = new Mock<IConvertService>();
            entryService.Setup(x => x.GetEntryValue(It.IsAny<string>())).Returns("TEN DOLLARS AND ONE CENT");

            // set controller object
            var controller = new ValuesController(entryService.Object, logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "1000"
            };

            #endregion

            // Act
            var actionResult = controller.Post(data);
            var contentResult = actionResult as OkNegotiatedContentResult<EntryResultViewModel>;

            // Assert
            Assert.IsNotNull(contentResult); // return data object should not be null
            Assert.IsNotNull(contentResult.Content); // return data content object should not be null
            Assert.IsTrue(contentResult.Content.Number.Length > 0); // returned number should not be empty
        }


        [TestMethod]
        public void GetReturnsWithNull()
        {
            // Arrange
            var logger = new Mock<ILog>();

            var entryService = new Mock<IConvertService>();
            entryService.Setup(x => x.GetEntryValue(It.IsAny<string>())).Returns(It.IsAny<string>);

            // set controller object
            var controller = new ValuesController(entryService.Object, logger.Object);

            #region //setting up fake data

            EntryViewModel data = null;

            #endregion

            // Act
            var actionResult = controller.Post(data);
            var contentResult = actionResult as OkNegotiatedContentResult<EntryResultViewModel>;

            // Assert
            Assert.IsNull(contentResult); // response data should be null

        }
    }
}
