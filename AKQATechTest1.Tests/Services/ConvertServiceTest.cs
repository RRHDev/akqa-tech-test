﻿using AKQATechTest1.Models;
using AKQATechTest1.Services;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AKQATechTest1.Tests.Services
{
    [TestClass]
    public class ConvertServiceTest
    {
        [TestMethod]
        public void TestIntegerNumbers()
        {
            // Arrange
            // Mock object for service
            var logger = new Mock<ILog>();

            // Init service
            var entryService = new ConvertService(logger.Object);
            
            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "1000"
            };

            #endregion

            // Act
            var result = entryService.GetEntryValue(data.Number);

            // Assert
            Assert.IsTrue(result == "ONE THOUSAND");
        }

        [TestMethod]
        public void TestNumbersWithDecimal()
        {
            // Arrange
            // Mock object for service
            var logger = new Mock<ILog>();

            // Init service
            var entryService = new ConvertService(logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "1000.10"
            };

            #endregion

            // Act
            var result = entryService.GetEntryValue(data.Number);

            // Assert
            Assert.IsTrue(result == "ONE THOUSAND AND TEN CENTS");
        }

        [TestMethod]
        public void TestNumbersWithDecimal2()
        {
            // Arrange
            // Mock object for service
            var logger = new Mock<ILog>();

            // Init service
            var entryService = new ConvertService(logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "1023.22"
            };

            #endregion

            // Act
            var result = entryService.GetEntryValue(data.Number);

            // Assert
            Assert.IsTrue(result == "ONE THOUSAND TWENTY THREE AND TWENTY-TWO CENTS");
        }


        [TestMethod]
        public void TestNegtiveNumbersWithDecimal()
        {
            // Arrange
            // Mock object for service
            var logger = new Mock<ILog>();

            // Init service
            var entryService = new ConvertService(logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "-1000.10"
            };

            #endregion

            // Act
            var result = entryService.GetEntryValue(data.Number);

            // Assert
            Assert.IsTrue(result == "MINUS ONE THOUSAND AND TEN CENTS");
        }

        [TestMethod]
        public void TestInvalidNumbers()
        {
            // Arrange
            // Mock object for service
            var logger = new Mock<ILog>();

            // Init service
            var entryService = new ConvertService(logger.Object);

            #region //setting up fake data

            var data = new EntryViewModel
            {
                Name = "Ray Zhang",
                Number = "non-numbers-string"
            };

            #endregion

            // Act
            var result = entryService.GetEntryValue(data.Number);

            // Assert
            Assert.IsTrue(result == "");
        }

    }
}
