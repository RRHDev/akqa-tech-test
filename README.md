# How to set up the project? 

* Open project solution file using Visual Studio 2017.
* Retore NuGet packages under Tools > NuGet Package Manager > Manage NuGet Package for Solution.
* Build solution once all packages has been restored.
* Start the project by pressing F5 or Ctrl+F5
* Enter name and number in the textbox fields
* Click submit, result will be shown underneath the submit button. 

# AKQA tech test project

* The project solution has 3 sub-projects.
	1. AKQATechTest1 project is web application project.
	2. AKQATechTest1.Services project is service layer that process the business logic and return.
	3. AKQATechTest1.Tests project is for unit tests
	

# Frameworks used:
* Denpendency injection container: Unity (Unity.WebApi) 
* Unit test framework: MSTest
* Mocking framework: Moq