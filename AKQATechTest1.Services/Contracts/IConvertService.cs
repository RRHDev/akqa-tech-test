﻿namespace AKQATechTest1.Services.Contracts
{
    public interface IConvertService
    {
        string GetEntryValue(string value);
    }
}
