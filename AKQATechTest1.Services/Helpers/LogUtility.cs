﻿using System;
using log4net;

namespace AKQATechTest1.Services.Helpers
{
    public static class LogUtility
    {
        public static ILog GetLogger(this object obj, string loggerName = "ErrorLog")
        {
            return LogManager.GetLogger(obj.GetType().Assembly, loggerName);
        }

        public static ILog GetLogger(Type objectType, string loggerName = "ErrorLog")
        {
            return LogManager.GetLogger(objectType.Assembly, loggerName);
        }

        public static ILog GetLogger<T>(string loggerName = "ErrorLog")
        {
            return LogManager.GetLogger(typeof(T).Assembly, loggerName);
        }
    }
}
