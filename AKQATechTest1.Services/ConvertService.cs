﻿using System;
using AKQATechTest1.Services.Contracts;
using log4net;

namespace AKQATechTest1.Services
{
    public class ConvertService : IConvertService
    {
        private readonly ILog _logger;
        /// <summary>
        /// Constractor
        /// </summary>
        /// <param name="logger"></param>
        public ConvertService(ILog logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// Get words by converting numbers
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string GetEntryValue(string number)
        {
            // Convert number to 'word'.

            var result = ConvertToWords(number);

            return result;
        }

        /// <summary>
        /// Convert ones to words
        /// </summary>
        /// <param name="numberString">string value of ones' number</param>
        /// <returns></returns>
        private static String ones(String numberString)
        {
            int number = Convert.ToInt32(numberString);
            String numberName = "";
            switch (number)
            {
                case 1:
                    numberName = "One";
                    break;
                case 2:
                    numberName = "Two";
                    break;
                case 3:
                    numberName = "Three";
                    break;
                case 4:
                    numberName = "Four";
                    break;
                case 5:
                    numberName = "Five";
                    break;
                case 6:
                    numberName = "Six";
                    break;
                case 7:
                    numberName = "Seven";
                    break;
                case 8:
                    numberName = "Eight";
                    break;
                case 9:
                    numberName = "Nine";
                    break;
            }
            return numberName;
        }

        /// <summary>
        /// Convert tens numbers to words
        /// </summary>
        /// <param name="numberString">string value of tens' number</param>
        /// <param name="isPointsNumber"></param>
        /// <returns></returns>
        static String tens(String numberString)
        {
            int number = Convert.ToInt32(numberString);
            String numberName = "";
            switch (number)
            {
                case 10:
                    numberName = "Ten";
                    break;
                case 11:
                    numberName = "Eleven";
                    break;
                case 12:
                    numberName = "Twelve";
                    break;
                case 13:
                    numberName = "Thirteen";
                    break;
                case 14:
                    numberName = "Fourteen";
                    break;
                case 15:
                    numberName = "Fifteen";
                    break;
                case 16:
                    numberName = "Sixteen";
                    break;
                case 17:
                    numberName = "Seventeen";
                    break;
                case 18:
                    numberName = "Eighteen";
                    break;
                case 19:
                    numberName = "Nineteen";
                    break;
                case 20:
                    numberName = "Twenty";
                    break;
                case 30:
                    numberName = "Thirty";
                    break;
                case 40:
                    numberName = "Fourty";
                    break;
                case 50:
                    numberName = "Fifty";
                    break;
                case 60:
                    numberName = "Sixty";
                    break;
                case 70:
                    numberName = "Seventy";
                    break;
                case 80:
                    numberName = "Eighty";
                    break;
                case 90:
                    numberName = "Ninety";
                    break;
                default:
                    if (number > 0)
                    {
                        var tensNum = tens(numberString.Substring(0, 1) + "0");
                        numberName = (tensNum == "0" ? " " : ((tensNum == "" ? "" : tensNum + "-"))) + ones(numberString.Substring(1));
                    }
                    break;
            }
            return numberName;
        }

        /// <summary>
        /// Convert numbers to words with unit appended if needed
        /// </summary>
        /// <param name="numberString"></param>
        /// <returns></returns>
        private String ConvertWholeNumber(String numberString)
        {
            var word = "";
            var negativeNumber = false;
            try
            {
                bool isDone = false;
                decimal decimalNumber = Convert.ToDecimal(numberString);
                int numberLength = numberString.Length;

                if (decimalNumber < 0)
                {
                    // check if it's a negative number
                    negativeNumber = true;
                    numberString = numberString.Substring(1);
                    numberLength--;
                }

                int pos = 0; //store digit grouping  
                String place = ""; //digit grouping name:hundres,thousand,etc...
                switch (numberLength)
                {
                    case 1: //ones
                        word = ones(numberString);
                        isDone = true;
                        break;
                    case 2: //tens
                        word = tens(numberString);
                        isDone = true;
                        break;
                    case 3: //hundreds
                        pos = (numberLength % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4: //thousands
                    case 5:
                    case 6:
                        pos = (numberLength % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 7: //millions
                    case 8:
                    case 9:
                        pos = (numberLength % 7) + 1;
                        place = " Million ";
                        break;
                    case 10: //Billions
                    case 11:
                    case 12:
                        pos = (numberLength % 10) + 1;
                        place = " Billion ";
                        break;
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {
                    //if transalation is not done, continue...
                    if (numberString.Substring(0, pos) != "0" && numberString.Substring(pos) != "0")
                    {
                        try
                        {
                            word = ConvertWholeNumber(numberString.Substring(0, pos)) + place + (place != "" ? "AND " : "") +
                                   ConvertWholeNumber(numberString.Substring(pos));
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.Message);
                        }
                    }
                    else
                    {
                        word = ConvertWholeNumber(numberString.Substring(0, pos)) +
                               ConvertWholeNumber(numberString.Substring(pos));
                    }

                }

                //ignore digit grouping names  

                if (word.Trim().Equals(place.Trim())) word = "";

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return (negativeNumber ? "MINUS " : "") + word.Trim();
        }

        /// <summary>
        /// Convert numbers to words
        /// </summary>
        /// <param name="numb"></param>
        /// <returns></returns>
        private String ConvertToWords(String numb)
        {
            var val = "";
            var digits = numb;
            var andStr = "";
            var pointStr = "";
            var endStr = "";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {

                    digits = numb.Substring(0, decimalPlace);
                    var points = numb.Substring(decimalPlace + 1);
                    var decimalPoints = "0." + points;
                    var roundedPoints = Math.Round(Convert.ToDecimal(decimalPoints) * 100);

                    var pointsInt = Convert.ToInt32(points);

                    if (pointsInt > 0)
                    {
                        andStr = "AND "; // separate whole numbers from dollars/cents  
                        endStr = $"Cent{(pointsInt > 1 ? "s" : "")}"; //Cents. 
                        pointStr = ConvertWholeNumber(roundedPoints.ToString());
                    }
                }

                var decDigits = Math.Round(Convert.ToDecimal(numb));

                // Append currency unit
                var dollarEndStr = $"{(decDigits == 0 ? "Zero " : "")}Dollar{(decDigits > 1 ? "s" : "")}"; //Dollars

                val = $"{ConvertWholeNumber(digits).Trim()} {dollarEndStr} {andStr}{pointStr} {endStr}".ToUpper();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return val.Trim();
        }

    }
}