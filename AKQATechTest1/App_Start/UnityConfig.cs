using AKQATechTest1.Services.Helpers;
using log4net;
using System.Web.Http;
using System.Web.Mvc;
using AKQATechTest1.Services.Contracts;
using Unity;
using Unity.Injection;
using Unity.WebApi;
using AKQATechTest1.Services;

namespace AKQATechTest1
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            // General
            container.RegisterType<ILog>(new InjectionFactory(factory => LogUtility.GetLogger<WebApiApplication>()));

            // Services
            container.RegisterType<IConvertService, ConvertService>();

        }
    }
}