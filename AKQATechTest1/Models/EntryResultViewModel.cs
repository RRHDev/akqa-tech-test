﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AKQATechTest1.Models
{
    public class EntryResultViewModel
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}