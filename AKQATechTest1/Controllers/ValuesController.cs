﻿using System;
using System.Net;
using System.Web.Http;
using AKQATechTest1.Models;
using AKQATechTest1.Services.Contracts;
using log4net;

namespace AKQATechTest1.Controllers
{
    public class ValuesController : ApiController
    {

        private readonly IConvertService _convertService;
        private readonly ILog _logger;

        public ValuesController(IConvertService convertService, ILog logger)
        {
            this._convertService = convertService;
            this._logger = logger;
        }

        // POST api/values
        public IHttpActionResult Post([FromBody]EntryViewModel entryViewModelValue)
        {
            var name = entryViewModelValue?.Name;
            var number = entryViewModelValue?.Number;

            // Check null/empty values
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(number))
            {
                //if one or both values are null/empty, return error message
                return BadRequest("Name and Number cannot be empty.");
            }

            try
            {
                // Invoke number converter method from _convertService.
                var numberInWords = this._convertService.GetEntryValue(entryViewModelValue.Number);

                // Return value
                var entryResult = new EntryResultViewModel
                {
                    Name = entryViewModelValue.Name,
                    Number = numberInWords
                };

                // Return response
                return Ok(entryResult);
            }
            catch (Exception ex)
            {
                // log error 
                _logger.Error(ex.Message);

                // Return server error response
                return StatusCode(HttpStatusCode.InternalServerError);
            }

        }

    }
}
